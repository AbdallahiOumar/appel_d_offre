package ISCAE.Appel_d_offre;

import ISCAE.Appel_d_offre.service.*;
import com.sun.jdi.LongValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Locale;

import static java.lang.String.valueOf;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AppelDOffreApplicationTests {

	@Autowired
	private Ao_service aoService;
	@Autowired
	private Company_service coservice;
	@Autowired
	private Representant_service RS;
	@Autowired
	Personne_p_service PS;
	@Autowired
	Application_service AS;
	@Autowired
	MockMvc mockMvc ;

	@Test
	void contextLoads() {
	}


	@Test
	void test1(){
		assertFalse(aoService.getAllAo().isEmpty());
	}

	@Test
	void test2(){
		assertFalse(coservice.findbyCompanynom("SNIM").getNom().toLowerCase(Locale.ROOT).equals("Snim"));
	}

	@Test
	void test3(){
		assertFalse(coservice.findbyCompanynom("SNIM").equals("snim@gmail.com"));
	}

	@Test
	void test4(){
		assertFalse(aoService.getAoById(1).getMontant().toString().isEmpty());
	}

	@Test
	void test5(){
		assertFalse(AS.AllApplication().isEmpty());
	}

	@Test
	void test6(){
		assertFalse(AS.FindAppbyAo(aoService.getAoById(1)).size()!=0);
	}

	@Test
	void test7(){
		assertFalse(AS.FindAppbyId(1).getObjet_ref().isEmpty());
	}

	@Test
	void test8(){
		assertFalse(AS.FindAppbyAoAndP(aoService.getAoById(1),PS.getPPById(1)).toString().isEmpty());
	}

	@Test
	void test9(){
		assertFalse(PS.getPPById(1).toString().isEmpty());
	}

	@Test
	void test10(){
		assertFalse(PS.getPPbyemail("a@gmail.com").toString().isEmpty());
	}






	@Test
	public void testGetAo() throws Exception {
		String res = mockMvc.perform(get("/Tous-les-appel-d-offre")
						.param("id", "2"))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		String Ao = valueOf(res);
		Assertions.assertFalse(Ao.isEmpty() );

	}

	@Test
	public void testAjou() throws Exception {
		int res = mockMvc.perform(get("/Ajouter-appel-d-offre"))
				.andExpect(status().is(400)).andReturn().getResponse().getStatus();
		Assertions.assertEquals(res,400);
	}

	@Test
	public void testGetCompany() throws Exception {
		String res = mockMvc.perform(get("/login")
						.param("id", "2"))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		String Co = valueOf(res);
		Assertions.assertFalse(Co.isEmpty());

	}

	@Test
	public void testDeconnection() throws Exception {
		int res = mockMvc.perform(get("/Deconnecter"))
				.andExpect(status().is(200)).andReturn().getResponse().getStatus();
		Assertions.assertEquals(res,200);
	}

	@Test
	public void testRegistre() throws Exception {
		int res = mockMvc.perform(get("/Creer-Compte"))
				.andExpect(status().is(200)).andReturn().getResponse().getStatus();
		Assertions.assertEquals(res,200);
	}

	@Test
	public void testSupprimer() throws Exception {
		int res = mockMvc.perform(get("/Supprimer-Appel-d-offre/1"))
				.andExpect(status().is(200)).andReturn().getResponse().getStatus();
		Assertions.assertEquals(res,200);
	}

	@Test
	public void testGetCompanyOffre() throws Exception {
		int res = mockMvc.perform(get("/Appel-d-offre-SNIM"))
				.andExpect(status().is(200)).andReturn().getResponse().getStatus();
		Assertions.assertEquals(res,200);
	}




}
