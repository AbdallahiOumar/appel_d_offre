package ISCAE.Appel_d_offre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppelDOffreApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppelDOffreApplication.class, args);
	}

}
