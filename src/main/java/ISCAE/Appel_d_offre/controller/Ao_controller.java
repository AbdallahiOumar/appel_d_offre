package ISCAE.Appel_d_offre.controller;


import ISCAE.Appel_d_offre.persistance.model.Ao;
import ISCAE.Appel_d_offre.persistance.model.Company;
import ISCAE.Appel_d_offre.service.Ao_service;
import ISCAE.Appel_d_offre.service.Company_service;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class Ao_controller {
    private final Ao_service AoService;
    private final Company_service CoService;
    public Ao_controller(Ao_service aoService, Company_service coService) {
        AoService = aoService;
        CoService = coService;
    }

    @GetMapping("/Ajouter-appel-d-offre")
    public String getAjoutAoPage(@RequestParam("nom") String nom, Model model){
        Company co = CoService.findbyCompanynom(nom);
        System.out.println(nom);
        model.addAttribute("AjoutAoRequest",new Ao());
        model.addAttribute("Company",co);
        return "AjoutAo_page";
    }

    @PostMapping("/Ajouter-appel-d-offre")
    public String AjoutAo(Model model,@ModelAttribute Ao ao,@RequestParam("nom") String nom){
        Company co = CoService.findbyCompanynom(nom);
        Ao a=AoService.SaveAo(ao.getObjet(),ao.getMontant(),ao.getDelai(),ao.getDate_fin(),co);

        if (a != null ){
            return "redirect:/Appel-d-offre-"+nom;
        } else {
            String e = "donnees incorecte";
            model.addAttribute("eror",e);
            return "AjoutAo_page";
        }
        }

    @GetMapping("/Supprimer-Appel-d-offre/{id}")
    public String getSupAoPage(Model model,@PathVariable int id){
        model.addAttribute("id",id);
        System.out.println(id);
        return "Supprimer_page";

    }

    @GetMapping("/Editer-appel-d-offre/{id}")
    public String getEditPage(Model mode,@PathVariable int id){
        Ao a=AoService.getAoById(id);
        mode.addAttribute(id);
        mode.addAttribute("Aos",a);
        return "Edit_page";
    }

   @PostMapping("/Supprimer-appel-d-offre")
    public String SupAo(@RequestParam("id") int id){
        Ao a=AoService.getAoById(id);
        AoService.deletAo(id);
        return "redirect:/Appel-d-offre-"+a.getCompany().getNom();

    }

    @PostMapping("/Editer-appel-d-offre")
    public String EditAo(Model model,@ModelAttribute Ao ao,@RequestParam("id") int id){
        //Company co = CoService.findbyCompanynom(id);
        Ao a=AoService.getAoById(id);
        a.setDelai(ao.getDelai());
        a.setDate_fin(ao.getDate_fin());
        a.setMontant(ao.getMontant());
        a.setObjet(ao.getObjet());
        AoService.save(a);

        if (a != null ){
            return "redirect:/Appel-d-offre-"+a.getCompany().getNom();
        } else {
            String e = "donnees incorecte";
            model.addAttribute("eror",e);
            return "Edit_page";
        }
    }

    @GetMapping("/Tous-les-appel-d-offre")
    public String getAolist(Model model){
        List<Ao> Aoss=AoService.getAllAo();
        List<Ao> Aos = new ArrayList<Ao>();
        for(Ao a : Aoss){
            if(a.getDate_fin().compareTo(Date.from(Instant.now()))>0){
               Aos.add(a);
            }
        }
        System.out.println(Aos);
        model.addAttribute("Aos",Aos);
        return "ListAo_page";
    }







}
