package ISCAE.Appel_d_offre.controller;

import ISCAE.Appel_d_offre.persistance.model.Ao;
import ISCAE.Appel_d_offre.persistance.model.Application;
import ISCAE.Appel_d_offre.persistance.model.Personne_p;
import ISCAE.Appel_d_offre.persistance.model.Representant;
import ISCAE.Appel_d_offre.service.Ao_service;
import ISCAE.Appel_d_offre.service.Application_service;
import ISCAE.Appel_d_offre.service.Personne_p_service;
import ISCAE.Appel_d_offre.service.Representant_service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Controller
public class Personne_p_controller {
    @Autowired
    private Personne_p_service PP_service;
    @Autowired
    private Ao_service aoservice;
    @Autowired
    private Application_service App_service;
    @Autowired
    Representant_service RS;

    @GetMapping("/soumettre-la-candidature/{id}")
    public String getCandidatPage(Model model, @PathVariable int id){
        model.addAttribute("AoId",id);
        System.out.println(aoservice.getAoById(id));
        return "Candidat_page";
    }

    @PostMapping("/soumettre-la-candidature")
    public String setCandidat(@ModelAttribute Personne_p p, @RequestParam("choix") String choix,@RequestParam("AoId") int AoId, Model model){
        PP_service.save(p.getNom(),p.getPrenom(),p.getEmail(),p.getAdresse(),p.getTelephone());

        for(Application App2:App_service.AllApplication()) {
            if (App2.getPersonneP() == PP_service.getPPbyemail(p.getEmail()) && App2.getAo() == aoservice.getAoById(AoId)) {
                return "Ereur";
            }
        }
        model.addAttribute("Email",p.getEmail());
        model.addAttribute("AoId",AoId);
        if(Objects.equals(choix, "Candidat libre")){
            return "Application_page";
        }else{
            return"Representant_page";
        }

    }

    @PostMapping("/soumettre-la-candidature-application")
    public String setCandidatApp(@ModelAttribute Application a, @RequestParam("AoId") int AoId,
                                 @RequestParam("Email") String Email, Model model){
        Ao ao=aoservice.getAoById(AoId);
        Personne_p p = PP_service.getPPbyemail(Email);
        System.out.println(ao);
        Application a2=App_service.saveApp(a.getObjet_ref(),a.getEntite_ref(),a.getMontant_ref(),a.getDate_ref(),p,ao);
        return "succes_page";

    }
    @PostMapping("/soumettre-la-candidature-Representant")
    public String Representant(@ModelAttribute Representant R, @RequestParam("AoId") int AoId, @RequestParam("Email") String Email, Model model){
        Personne_p p = PP_service.getPPbyemail(Email);
        Representant R2= RS.saveRP(R.getDenomination(),R.getImmatriculation(),p);
        model.addAttribute("Email",p.getEmail());
        model.addAttribute("AoId",AoId);
        return "Application_page";
    }

    @GetMapping("/candidat-d-appel-d-offre/{id}")
    public String getCandidatListe(@PathVariable int id,Model model){
        Ao ao=aoservice.getAoById(id);
        List<Application> App=App_service.FindAppbyAo(ao);
        model.addAttribute("App",App);
        return "Candidats_Liste_Page";
    }

    @GetMapping("/Choisir/{id}")
    public String Selectionner(@PathVariable int id, Model model){
        model.addAttribute("id",id);
        return "selection_succes_page";
    }

    @PostMapping("/Choisir-candidat")
    public String setSelectionner(@RequestParam("id") int id){

        Application App=App_service.FindAppbyId(id);

        App.getAo().setEmail_personne(App.getPersonneP().getEmail());
        App.getAo().setDate_att(Date.from(Instant.now()));
        aoservice.UpdateAo(App.getAo().getId(),App.getAo());
        System.out.println(App);
        return "redirect:/Appel-d-offre-"+App.getAo().getCompany().getNom();
    }

    @GetMapping("/infos/{id}")
    public String infos(@PathVariable int id,Model model){
        Ao a=aoservice.getAoById(id);
        Personne_p p=PP_service.getPPbyemail(a.getEmail_personne());
        Application App = App_service.FindAppbyAoAndP(a,p);
        model.addAttribute("App",App);
        return "selection_page";
    }


}
