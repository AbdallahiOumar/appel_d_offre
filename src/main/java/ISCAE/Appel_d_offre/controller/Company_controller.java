package ISCAE.Appel_d_offre.controller;

import ISCAE.Appel_d_offre.persistance.model.Company;
import ISCAE.Appel_d_offre.service.Company_service;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Company_controller {

    private final Company_service Company_service;

    public Company_controller(ISCAE.Appel_d_offre.service.Company_service company_service) {
        this.Company_service = company_service;
    }

    @GetMapping("/Creer-Compte")
    public String getregistrepage(Model model){
        model.addAttribute("registreRequest",new Company());
        return  "registre_page";
    }

    @GetMapping("/login")
    public String getloginpage(Model model){
        model.addAttribute("loginRequest",new Company());
        return  "login_page";
    }

    @GetMapping("Deconnecter")
    public String getlogoutpage(){
        return  "login_page";
    }

    @GetMapping("/Appel-d-offre-{company}")
    public String getcompanypage(Model model, @PathVariable String company){
        Company c = Company_service.findbyCompanynom(company);
        model.addAttribute("companyAos",this.Company_service.findbyCompany(c));
        model.addAttribute("company",c.getNom());
        System.out.println(company);
        return  "company_page";
    }

    @GetMapping("/appel-d-offre/{company}")
    public String getcompanyAos(Model model, @PathVariable String company){
        Company c = Company_service.findbyCompanynom(company);
        model.addAttribute("companyAos",this.Company_service.findbyCompany(c));
        model.addAttribute("company",c.getNom());
        System.out.println(company);
        return  "companyAos_page";
    }

    @PostMapping("/Creer-Compte")
    public String registre(@ModelAttribute Company company, Model model){
        System.out.println("registre request: "+company);
        Company registredCompany = Company_service.SaveCompany(company.getEmail(),company.getLocation(),company.getPassword(),company.getDescription(),company.getNom(),company.getTelephone());
        if (registredCompany != null ){
            return "redirect:/login";
        } else {
            String e = "Entreprise existe deja";
            model.addAttribute("eror",e);
            return "registre_page";
        }
    }

    @PostMapping("/login")
    public String login(@ModelAttribute Company company, Model model){
        System.out.println("login request: "+company);
        Company authentificatedCompany = Company_service.Authentificated(company.getEmail(),company.getPassword());
        if(authentificatedCompany != null){
            System.out.println("CompanyAos: "+this.Company_service.findbyCompany(authentificatedCompany));
            model.addAttribute("CompanyLogin", authentificatedCompany.getNom());
            return "redirect:/Appel-d-offre-"+authentificatedCompany.getNom();
        }else{
            String erreur = "Email ou Mot de passe incorrecte";
            model.addAttribute("Erreur", erreur);
            return "login_page";
        }
    }
}
