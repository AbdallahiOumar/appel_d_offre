package ISCAE.Appel_d_offre.persistance.repository;
import ISCAE.Appel_d_offre.persistance.model.Application;
import org.springframework.data.jpa.repository.JpaRepository;

@org.springframework.stereotype.Repository
public interface Application_repository extends JpaRepository<Application,Integer> {
}
