package ISCAE.Appel_d_offre.persistance.repository;
import ISCAE.Appel_d_offre.persistance.model.Ao;
import org.springframework.data.jpa.repository.JpaRepository;



@org.springframework.stereotype.Repository
public interface Ao_repository extends JpaRepository<Ao,Integer> {

}
