package ISCAE.Appel_d_offre.persistance.repository;


import ISCAE.Appel_d_offre.persistance.model.Personne_p;
import org.springframework.data.jpa.repository.JpaRepository;

@org.springframework.stereotype.Repository
public interface Personne_p_repository extends JpaRepository<Personne_p,Integer> {
}
