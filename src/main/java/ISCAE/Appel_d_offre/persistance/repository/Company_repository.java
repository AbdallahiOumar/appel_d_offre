package ISCAE.Appel_d_offre.persistance.repository;

import ISCAE.Appel_d_offre.persistance.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

@org.springframework.stereotype.Repository
public interface Company_repository extends JpaRepository<Company,Integer> {


    Optional<Company> findByEmailAndPassword(String email, String password);


}