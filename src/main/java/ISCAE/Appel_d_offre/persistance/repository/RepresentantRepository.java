package ISCAE.Appel_d_offre.persistance.repository;

import ISCAE.Appel_d_offre.persistance.model.Representant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepresentantRepository extends JpaRepository<Representant,Integer> {

}
