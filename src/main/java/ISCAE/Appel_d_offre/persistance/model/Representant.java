package ISCAE.Appel_d_offre.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Representant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int R_id;
    String denomination;
    Long immatriculation;
    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "personne_id")
    Personne_p p;

    @Override
    public String toString() {
        return "Representant{" +
                "R_id=" + R_id +
                ", denomination='" + denomination + '\'' +
                ", immatriculation=" + immatriculation +
                ", p=" + p.getNom() +
                '}';
    }
}


