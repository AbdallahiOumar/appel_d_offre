package ISCAE.Appel_d_offre.persistance.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Getter
    @Setter
    @NotNull
    @Column(name = "objet_ref", nullable = false)
    String objet_ref;

    @Getter
    @Setter
    @NotNull
    @Column(name = "montant_ref", nullable = false)
    Double montant_ref;

    @Getter
    @Setter
    @NotNull
    @Column(name = "entite_ref", nullable = false)
    String entite_ref;

    @Getter
    @Setter
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_ref", nullable = false)
    Date date_ref;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "personne_id",nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Personne_p personneP;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "ao_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Ao ao;

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", objet_ref='" + objet_ref + '\'' +
                ", montant_ref=" + montant_ref +
                ", entite_ref='" + entite_ref + '\'' +
                ", date_ref=" + date_ref +
                ", personneP=" + personneP +
                ", ao=" + ao +
                '}';
    }
}
