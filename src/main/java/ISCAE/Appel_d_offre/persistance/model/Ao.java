package ISCAE.Appel_d_offre.persistance.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Ao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ao_id", nullable = false)
    private Integer id;

    @Getter
    @Setter
    @NotNull
    @Column(name = "objet", nullable = false)
    String objet;

    @Getter
    @Setter
    @NotNull
    @Column(name = "montant", nullable = false)
    Double montant;

    @Getter
    @Setter
    @NotNull
    @Column(name = "delai", nullable = false)
    String delai;

    @Getter
    @Setter
    @Column(name = "email_personne")
    String Email_personne;

    @Getter
    @Setter
    @NotNull
    @Column(name = "date_pub", nullable = false, updatable = false)
    Date date_pub;

    @Getter
    @Setter
    @Column(name = "date_att")
    Date date_att;

    @Getter
    @Setter
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "date_fin", nullable = false)
    Date date_fin;

    @Getter
    @Setter
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "company_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    Company Company;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ao ao = (Ao) o;
        return Objects.equals(id, ao.id) && Objects.equals(objet, ao.objet) && Objects.equals(montant, ao.montant) && Objects.equals(delai, ao.delai) && Objects.equals(date_pub, ao.date_pub) && Objects.equals(date_fin, ao.date_fin) && Objects.equals(Company, ao.Company) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, objet, montant, delai, date_pub, date_fin, Company);
    }


    @Override
    public String toString() {
        return "Ao{" +
                "id=" + id +
                ", objet='" + objet + '\'' +
                ", montant=" + montant +
                ", delai='" + delai + '\'' +
                ", Email_personne='" + Email_personne + '\'' +
                ", date_pub=" + date_pub +
                ", date_att=" + date_att +
                ", date_fin=" + date_fin +
                ", Company=" + Company.getNom() +
                '}';
    }
}
