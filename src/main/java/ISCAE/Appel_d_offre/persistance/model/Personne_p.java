package ISCAE.Appel_d_offre.persistance.model;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Personne_p {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "personne_id", nullable = false)
    private Integer Id;

    @Getter
    @Setter
    @NotNull
    @Column(name = "nom", nullable = false)
    String nom;

    @Getter
    @Setter
    @NotNull
    @Column(name = "Prenom", nullable = false)
    String prenom;

    @Getter
    @Setter
    @NotNull
    @Column(name = "email", nullable = false)
    String email;

    @Getter
    @Setter
    @NotNull
    @Column(name = "telephone", nullable = false)
    int telephone;

    @Getter
    @Setter
    @NotNull
    @Column(name = "adresse", nullable = false)
    String adresse;

    @OneToOne(mappedBy = "p", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Representant R;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne_p that = (Personne_p) o;
        return telephone == that.telephone && Objects.equals(Id, that.Id) && Objects.equals(nom, that.nom) && Objects.equals(prenom, that.prenom) && Objects.equals(email, that.email) && Objects.equals(adresse, that.adresse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, nom, prenom, email, telephone, adresse);
    }

    @Override
    public String toString() {
        return "Personne_p{" +
                "Id=" + Id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", email='" + email + '\'' +
                ", telephone=" + telephone +
                ", adresse='" + adresse + '\'' +
                '}';
    }
}

