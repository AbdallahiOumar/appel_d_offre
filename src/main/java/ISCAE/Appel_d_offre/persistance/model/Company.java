package ISCAE.Appel_d_offre.persistance.model;

import com.sun.istack.NotNull;

import lombok.*;
;

import javax.persistence.*;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;


    @Getter
    @Setter
    @NotNull
    @Column(name = "nom", nullable = false)
    String nom;

    @Getter
    @Setter
    @NotNull
    @Column(name = "telephone", nullable = false)
    int telephone;

    @Getter
    @Setter
    @NotNull
    @Column(name = "Description", nullable = false)
    String Description;


    @Getter
    @Setter
    @NotNull
    @Column(name = "email", nullable = false)
    String email;

    @Getter
    @Setter
    @NotNull
    @Column(name = "location", nullable = false)
    String location;

    @Getter
    @Setter
    @NotNull
    @Column(name = "password", nullable = false)
    String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return Objects.equals(id, company.id) && Objects.equals(nom, company.nom) && Objects.equals(Description, company.Description) && Objects.equals(email, company.email) && Objects.equals(location, company.location) && Objects.equals(password, company.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, Description, email, location, password);
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", Description='" + Description + '\'' +
                ", email='" + email + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

}
