package ISCAE.Appel_d_offre.service;

import ISCAE.Appel_d_offre.persistance.model.Ao;
import ISCAE.Appel_d_offre.persistance.model.Company;
import ISCAE.Appel_d_offre.persistance.repository.Ao_repository;
import ISCAE.Appel_d_offre.persistance.repository.Company_repository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Service
public class Ao_service {
    private final Ao_repository AoRepository;
    private final Company_repository CompanyRepository;


    public Ao_service(Ao_repository aoRepository, Company_repository companyRepository, Company_service companyservice) {
        this.AoRepository = aoRepository;
        this.CompanyRepository = companyRepository;

    }

    public Ao SaveAo(String objet, Double montant, String delai, Date date_fin, Company Company) {
        if (objet == null || delai == null || montant == null || date_fin == null || Company == null ) {
            return null;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy 'à' HH:mm:ss");
            Ao Ao = new Ao();
            Ao.setDelai(delai);
            Ao.setCompany(Company);
            Ao.setDate_fin(date_fin);
            Ao.setMontant(montant);
            Ao.setObjet(objet);
            Ao.setDate_pub(Date.from(Instant.now()));
            return AoRepository.save(Ao);
        }
    }

    public void UpdateAo(int id, Ao Ao) {
        Ao Aos = AoRepository.findById(id).get();
        Aos.setObjet(Ao.getObjet());
        Aos.setMontant(Ao.getMontant());
        Aos.setDelai(Ao.getDelai());
        Aos.setDate_fin(Ao.getDate_fin());
        AoRepository.save(Aos);
    }

    public List<Ao> getAllAo(){

        return AoRepository.findAll(Sort.by("id").descending());
    }

    public Ao getAoById(int id) {
        List<Ao> Ao= AoRepository.findAll();
        Ao ao2=new Ao();
        for ( Ao a : Ao){
            if(a.getId()==id){
                ao2=a;
            }
        }
        return ao2;
    }



        public void deletAo ( int id){
            AoRepository.deleteById(id);
        }

    public void save ( Ao ao){
        AoRepository.save(ao);
    }


    }

