package ISCAE.Appel_d_offre.service;


import ISCAE.Appel_d_offre.persistance.model.Ao;
import ISCAE.Appel_d_offre.persistance.model.Application;
import ISCAE.Appel_d_offre.persistance.model.Personne_p;
import ISCAE.Appel_d_offre.persistance.repository.Application_repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class Application_service {
    @Autowired
    private Application_repository App_repository;

    public Application saveApp (String obj_ref, String entite_ref, Double montant_ref, Date date_ref, Personne_p p, Ao ao){

        if(obj_ref==null || entite_ref==null || montant_ref==null || date_ref==null || p==null || ao==null){
            return null;
        }
        for(Application App2:App_repository.findAll()) {
            if (App2.getPersonneP() == p && App2.getAo() == ao)
                return null;
        }
        Application App = new Application();
        App.setAo(ao);
        App.setDate_ref(date_ref);
        App.setEntite_ref(entite_ref);
        App.setMontant_ref(montant_ref);
        App.setPersonneP(p);
        App.setObjet_ref(obj_ref);
        return App_repository.save(App);
    }

    public List<Application> AllApplication(){
        return App_repository.findAll();
    }

    public List<Application> FindAppbyAo(Ao a){
        List<Application> ListApp =new ArrayList<Application>();
        for(Application App:App_repository.findAll()){
            if(App.getAo()==a){
                ListApp.add(App);
            }
        }
        return ListApp;
    }

    public Application FindAppbyId(int Id){
        Application App =new Application();
        for(Application App2:App_repository.findAll()){
            if(App2.getId()==Id){
                App.setAo(App2.getAo());
                App.setDate_ref(App2.getDate_ref());
                App.setPersonneP(App2.getPersonneP());
                App.setEntite_ref(App2.getEntite_ref());
                App.setObjet_ref(App2.getObjet_ref());
                App.setMontant_ref(App2.getMontant_ref());

            }
        }
        return App;
    }
    public Application FindAppbyAoAndP(Ao a,Personne_p p){
        Application App =new Application();
        for(Application App2:App_repository.findAll()){
            if(App2.getAo()==a && App2.getPersonneP()==p){
                App.setAo(App2.getAo());
                App.setDate_ref(App2.getDate_ref());
                App.setPersonneP(App2.getPersonneP());
                App.setEntite_ref(App2.getEntite_ref());
                App.setObjet_ref(App2.getObjet_ref());
                App.setMontant_ref(App2.getMontant_ref());

            }
        }
        return App;
    }


}
