package ISCAE.Appel_d_offre.service;

import ISCAE.Appel_d_offre.persistance.model.Application;
import ISCAE.Appel_d_offre.persistance.model.Company;
import ISCAE.Appel_d_offre.persistance.model.Personne_p;
import ISCAE.Appel_d_offre.persistance.model.Representant;
import ISCAE.Appel_d_offre.persistance.repository.RepresentantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Representant_service {

    @Autowired
    RepresentantRepository RP;

    public Representant saveRP(String denominatiom, Long immatriculation, Personne_p p){
        if(denominatiom==null || immatriculation==null || p==null){
            return null;
        }
        for(Representant R : RP.findAll()) {
            if (R.getP() ==p || R.getImmatriculation()==immatriculation)
                return null;
        }
        Representant R2 = new Representant();
       R2.setDenomination(denominatiom);
       R2.setImmatriculation(immatriculation);
       R2.setP(p);
       return RP.save(R2);
    }

    public Representant findRp(Long immatriculation){
        List<Representant> RS= RP.findAll();
        Representant R2=new Representant();
        for ( Representant a : RS){
            if(a.getImmatriculation()==immatriculation){
                R2=a;
            }
        }
        return R2;
    }
}
