package ISCAE.Appel_d_offre.service;

import ISCAE.Appel_d_offre.persistance.model.Ao;
import ISCAE.Appel_d_offre.persistance.model.Company;
import ISCAE.Appel_d_offre.persistance.repository.Ao_repository;
import ISCAE.Appel_d_offre.persistance.repository.Company_repository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class Company_service {
    private final Company_repository CompanyRepository;
    private final Ao_repository AoRepository;


    public Company_service(Company_repository companyRepository,Ao_repository aoRepository) {
        this.CompanyRepository = companyRepository;
        this.AoRepository = aoRepository;
    }

    public Company SaveCompany(String email, String location, String password, String description, String nom, int telephone){
        if(email==null || password==null || description==null || nom==null || location==null || telephone==0){
            return null;
        }else{
            List<Company> Companies = this.CompanyRepository.findAll();
            for(Company C : Companies) {
                if (Objects.equals(C.getEmail(), email) || Objects.equals(C.getNom(), nom)) {
                    System.out.println("L'entreprise existe deja !!");
                    return null;
                }
            }

            Company Company = new Company();
            Company.setTelephone(telephone);
            Company.setDescription(description);
            Company.setEmail(email);
            Company.setLocation(location);
            Company.setNom(nom);
            Company.setPassword(password);
            return CompanyRepository.save(Company);
        }

    }

    public List<Ao> findbyCompany(Company c) {
        List<Ao> Aos= AoRepository.findAll(Sort.by("id").descending());
        List<Ao> ComAos = new ArrayList<Ao>();

        for ( Ao a : Aos){

            if(a.getCompany().getId()==c.getId()){
                ComAos.add(a);
            }
        }

        return ComAos;

    }
    public Company findbyCompanynom(String nom) {
        List<Company> companies= CompanyRepository.findAll();
        Company a2=new Company();
        for ( Company a : companies){
            if(a.getNom().equals(nom)){
                a2=a;
            }
        }
        return a2;
    }

    public Company findbyCompanyid(int id) {
        List<Company> companies= CompanyRepository.findAll();
        Company a2=new Company();
        for ( Company a : companies){
            if(a.getId()==id){
                a2=a;
            }
        }
        return a2;
    }

    public Company Authentificated (String email, String password){
        return CompanyRepository.findByEmailAndPassword(email,password).orElse(null);

    }

}
