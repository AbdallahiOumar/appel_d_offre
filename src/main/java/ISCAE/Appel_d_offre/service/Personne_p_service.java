package ISCAE.Appel_d_offre.service;

import ISCAE.Appel_d_offre.persistance.model.Personne_p;
import ISCAE.Appel_d_offre.persistance.repository.Ao_repository;
import ISCAE.Appel_d_offre.persistance.repository.Personne_p_repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class Personne_p_service {
    @Autowired
    private Personne_p_repository PP_repository;
    @Autowired
    private Ao_repository aoRepository;

    public Personne_p save(String nom, String prenom, String email, String adresse, int telephone){
        if(nom== null || prenom==null || email== null || adresse==null || telephone==0){
            return null;
        }else{
            List<Personne_p> personnes = this.PP_repository.findAll();
            for(Personne_p p : personnes) {
                if (Objects.equals(p.getEmail(), email) || p.getTelephone()==telephone ) {
                    return null;
                }
            }
        }
        Personne_p p=new Personne_p();
        p.setAdresse(adresse);
        p.setEmail(email);
        p.setNom(nom);
        p.setTelephone(telephone);
        p.setPrenom(prenom);
        return PP_repository.save(p);
    }

    public Personne_p getPPbyemail(String email){
        List<Personne_p> personnes=PP_repository.findAll();
        Personne_p p =new Personne_p();
        for(Personne_p p1 : personnes){
            if(p1.getEmail().equals(email)){
                p=p1;
            }
        }
        return p;
    }

    public Personne_p getPPById(int id) {
        List<Personne_p> pp1= PP_repository.findAll();
        Personne_p pp2=new Personne_p();
        for ( Personne_p pp3 : pp1){
            if(pp3.getId()==id){
                pp2=pp3;
            }
        }
        return pp2;
    }


}
